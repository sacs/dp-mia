import torch
import torchvision.transforms as transforms
from torchvision.datasets import MNIST
import random as rd

MNIST_MEAN = 0.1307
MNIST_STD = 0.3081
transform = transforms.Compose([
    transforms.ToTensor(),
    transforms.Normalize(MNIST_MEAN, MNIST_STD),
])
DATA_ROOT = '../mnist'
train_dataset = MNIST(
    root=DATA_ROOT, train=True, download=True, transform=transform)

def get_subset_training(subset_size = 10000, batch_size = 512):
    '''
    Returns a loader with a subset of the MNIST training dataset.

    Arguments:
        subset_size (int): size of the subset that will be returned.
        batch_size (int): how many samples per batch to load.

    Returns:
        DataLoader: a PyTorch object with the subset of the data.
    '''
    s = rd.sample(list(range(len(train_dataset))), subset_size)
    ts = torch.utils.data.Subset(train_dataset, s)
    loader = torch.utils.data.DataLoader(ts, batch_size = batch_size, pin_memory = True)
    return loader

def get_test(batch_size = 512):
    '''
    Returns a loader with the MNIST test dataset.

    Arguments:
        batch_size (int): how many samples per batch to load.

    Returns:
        DataLoader: a PyTorch object with the test data.
    '''
    dataset = MNIST(
        root=DATA_ROOT, train=False, download=True, transform=transform)
    loader = torch.utils.data.DataLoader(
        dataset,
        batch_size = batch_size,
        shuffle=False,
    )
    return loader
