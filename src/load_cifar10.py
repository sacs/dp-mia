import torch
import torchvision.transforms as transforms
from torchvision.datasets import CIFAR10
import random as rd

CIFAR10_MEAN = (0.4914, 0.4822, 0.4465)
CIFAR10_STD_DEV = (0.2023, 0.1994, 0.2010)
transform = transforms.Compose([
    transforms.ToTensor(),
    transforms.Normalize(CIFAR10_MEAN, CIFAR10_STD_DEV),
])
DATA_ROOT = '../cifar10'
train_dataset = CIFAR10(
    root=DATA_ROOT, train=True, download=True, transform=transform)

def get_subset_training(subset_size = 10000, batch_size = 512):
    '''
    Returns a loader with a subset of the CIFAR10 training dataset.

    Arguments:
        subset_size (int): size of the subset that will be returned.
        batch_size (int): how many samples per batch to load.

    Returns:
        DataLoader: a PyTorch object with the subset of the data.
    '''
    s = rd.sample(list(range(len(train_dataset))), subset_size)
    ts = torch.utils.data.Subset(train_dataset, s)
    loader = torch.utils.data.DataLoader(ts, batch_size = batch_size, pin_memory = True)
    return loader

def get_test(batch_size = 512):
    '''
    Returns a loader with the CIFAR10 test dataset.

    Arguments:
        batch_size (int): how many samples per batch to load.

    Returns:
        DataLoader: a PyTorch object with the test data.
    '''
    dataset = CIFAR10(
        root=DATA_ROOT, train=False, download=True, transform=transform)
    loader = torch.utils.data.DataLoader(
        dataset,
        batch_size = batch_size,
        shuffle=False,
    )
    return loader
