from model_cifar10 import SampleConvNet
from load_cifar10 import get_test
from small_world_graph import get_graph
from node import Node
from attack import attack
from itertools import product
from time import time
import numpy as np
import random as rd
import torch
import pandas as pd
from parser import parser, parse_lists
from utils import attack_list, metrics

torch.manual_seed(parser.seed)
np.random.seed(parser.seed)
rd.seed(parser.seed)
norms = parse_lists(parser.max_norms)
noises = parse_lists(parser.noise_multipliers)

graph = get_graph(n = parser.number_nodes)
graph = [[2, 4], [2, 3, 4], [0, 1], [1, 4], [0, 1, 3]]
n_subnodes = [3, 3, 4, 2, 4]
n_graph, total_subnodes = len(graph), sum(n_subnodes)
test_loader = get_test()

columns = ['iter', 'loss', 'accuracy', 'convergence_score', 'time', 'epsilon', 'best_alpha', 
           'auc_attack_before', 'auc_attack_after', 'max_norm', 'noise_multiplier']

results = []
start = time()
for idx, (norm, noise) in enumerate(product(norms, noises)):
    print(f'Start of parameter {idx+1}/{len(norms)*len(noises)}')
    subnodes = [Node(SampleConvNet, max_grad_norm = norm, noise_multiplier = noise,
                  training_size = 10000, dp = not parser.disable_dp) for _ in range(total_subnodes)]
    nodes = [Node(SampleConvNet, dp = not parser.disable_dp) for _ in range(n_graph)]

    for iter_ in range(parser.n_iter):
        log = iter_ % 5 == 4 or parser.log_everything
        print(f'Iter: {iter_ + 1}/{parser.n_iter}')
        params = {}
        losses = []

        #train the all subnodes
        for i in range(total_subnodes):
            losses.append(subnodes[i].train(epochs = parser.epochs))
            params[i] = subnodes[i].get_state_dict()
        
        #average subnodes
        for i in range(n_graph):
            curr_params = []
            for j in range(sum(n_subnodes[:i]), sum(n_subnodes[:i+1])):
                curr_params.append(params[j])
            nodes[i].update_params(curr_params, include_own = False)

        #attack nodes before
        if log:
            attacks_bfr = attack_list(subnodes, test_loader, parser.enable_attacks)

        #update nodes
        params = [nodes[i].get_state_dict() for i in range(n_graph)]
        for i in range(n_graph):
            curr_params = []
            for j in graph[i]:
                curr_params.append(params[j])
            nodes[i].update_params(curr_params)

        #update subnodes
        for i in range(n_graph):
            for j in range(sum(n_subnodes[:i]), sum(n_subnodes[:i+1])):
                subnodes[j].update_params([nodes[i].get_state_dict()], include_own = False)

        #measure metrics
        if log: 
            attacks_aft = attack_list(subnodes, test_loader, parser.enable_attacks)
            accs, eps, alphas, conv = metrics(subnodes, test_loader)
            curr_result = [iter_+1, np.mean(losses), np.mean(accs), conv, time() - start, np.mean(eps),
                           np.mean(alphas), np.mean(attacks_bfr), np.mean(attacks_aft), norm, noise]
            results.append(curr_result)
            df = pd.DataFrame(results, columns = columns)
            df.to_csv(parser.results_file, index = False)

    print('---------------------------\n')
