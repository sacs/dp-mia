import torch
from torchvision import models
from opacus.validators import ModuleValidator
import torch.nn as nn
import torch.optim as optim
from opacus.accountants import RDPAccountant
from opacus.optimizers import DPOptimizer
from opacus import GradSampleModule
from tqdm import tqdm
import numpy as np
from load_cifar10 import get_subset_training, get_test 

from inspect import signature

class Node:
    '''
    Class that represents one machine in a decentralized learning framework.

    Attributes:
        get_model (function): function that returns a PyTorch model.
        training_size (int, optional): size of the nodes training dataset.
        batch_size (int, optional): how many samples per batch to load.
        dp (bool, optional): if the model will use differential privacy.
        device (str, optional): 'cpu' or 'cuda', device where the model runs.
        lr (float, optional): learning rate of the model.
        delta (float, optional): delta of almost differential privacy.
        max_grad_norm (float, optional): max_grad_norm used for gradient clipping.
        noise_multiplier(float, optional): noise_multiplier of the DP-SGD algorithm.
    '''
    def __init__(self, get_model, training_size = 10000, batch_size = 500, dp = True,
                 device = 'cpu', lr = 0.05, delta = None, max_grad_norm = 1.0, noise_multiplier = 1.0):
        self.train_loader = get_subset_training(training_size, batch_size)
        self.model = get_model()
        self.dp = dp
        self.device = torch.device(device)
        self.optimizer = optim.SGD(self.model.parameters(), lr = lr)
        if delta is None:
            self.delta = 1.0/len(self.train_loader.dataset)
        else:
            self.delta = delta
        if dp:
            self.accountant = RDPAccountant()
            self.model = GradSampleModule(self.model)
            self.optimizer = DPOptimizer(optimizer = self.optimizer, 
                                         noise_multiplier = noise_multiplier,
                                         max_grad_norm = max_grad_norm,
                                         expected_batch_size = batch_size)
            self.optimizer.attach_step_hook(
                self.accountant.get_optimizer_hook_fn(
                    sample_rate = batch_size/len(self.train_loader.dataset)))
            assert len(ModuleValidator.validate(self.model, strict = False)) == 0
        self.lens = []
        self.shapes = []
        with torch.no_grad():
            for _, v in self.model.state_dict().items():
                self.shapes.append(v.shape)
                t = v.flatten().numpy()
                self.lens.append(t.shape[0])

    def train(self, epochs = 1):
        '''
        Performs one round of training on local data.

        Arguments:
            epochs (int, optional): how many iterations through the data.

        Return:
            np.array: mean loss of last epoch.
        '''
        self.model.to(self.device)
        self.model.train()
        criterion = nn.CrossEntropyLoss()
        for _ in range(epochs):
            losses = []
            for data, target in self.train_loader:
                data, target = data.to(self.device), target.to(self.device)
                self.optimizer.zero_grad()
                output = self.model(data)
                loss = criterion(output, target)
                loss.backward()
                self.optimizer.step()
                losses.append(loss.item())
        if self.dp:
            epsilon, best_alpha = self.accountant.get_privacy_spent(delta = self.delta)
            print(f"Loss: {np.mean(losses):.6f} (ε = {epsilon:.2f}, δ = {self.delta}) for α = {best_alpha}")
        else:
            print(f"Loss: {np.mean(losses):.6f}")
        return np.mean(losses)

    def get_privacy(self, delta = None):
        '''
        Compute the privacy budget spent.

        Arguments:
            delta (float, optional): delta of almost differencia privacy.

        Returns:
            float: the privacy budget spent.
        '''
        if not self.dp:
            return np.nan, np.nan
        if delta is None:
            delta = self.delta
        epsilon, best_alpha = self.accountant.get_privacy_spent(delta = delta)
        return epsilon, best_alpha

    def get_state_dict(self):
        '''
        Get dictionary with all parameters.
        '''
        return self.model.state_dict()

    def update_params(self, state_dicts, include_own = True):
        '''
        Update all parameters of the CNN.

        Arguments:
            state_dicts (list of dicts): list of neighbors parameters.
            include_own (bool, optional): if the average of models should include own model.
        '''
        n = len(state_dicts)
        own = self.get_state_dict()
        if not include_own:
            for i in own:
                own[i] = 0
        for i in state_dicts[0]:
            for j in range(n):
                own[i] += state_dicts[j][i]
            own[i] = own[i] / (n+int(include_own))
        self.model.load_state_dict(own)

    def get_serialized_model(self):
        '''
        Get all parameters in a numpy array.

        Returns:
            numpy.array: array with flattenned parameters.
        '''
        to_cat = []
        with torch.no_grad():
            for _, v in self.model.state_dict().items():
                t = v.flatten()
                to_cat.append(t)
        flat = torch.cat(to_cat)
        data = dict()
        data["params"] = flat.numpy()
        return data

    def test(self, test_loader):
        '''
        Test accuracy on test dataset.

        Arguments:
            test_loader: PyTorch data loader with the test dataset.

        Returns:
            float: accuracy of the model on the test dataset.
        '''
        criterion = torch.nn.CrossEntropyLoss()
        with torch.no_grad():
            n_correct = 0
            for data, target in test_loader:
                data, target = data.to(self.device), target.to(self.device)
                output = self.model(data)
                predicted = output.argmax(dim=1, keepdim=True).numpy()
                n_correct += sum(predicted.T[0] == target.numpy())
        sz = len(test_loader.dataset)
        print(f'Test Acc: {100.0 * n_correct / sz}%')
        return n_correct / sz


    def talk(self):
        '''
        Get first weight of the CNN for debugging purposes.

        Returns:
            float: first weight of the CNN.
        '''
        p = self.get_serialized_model()
        return p['params'][0]
