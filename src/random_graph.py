import networkx as nx
import random as rd

def get_graph(n = 16, degree = 4, seed = 1):
    '''
    Get random graph.

    Arguments:
        n (int, optional): number of nodes.
        degree (int, optional): expected degree of each node
        seed (int, optional): seed to random part of the algorithm.

    Returns:
        list: adjacency list representation of the generated graph.
    '''
    g = nx.erdos_renyi_graph(n, degree / (n-1), seed = seed)
    while not nx.is_connected(g):
        g = nx.erdos_renyi_graph(n, degree / (n-1), seed = rd.randint(0, 2**30))
    return [list(g[i]) for i in g]
