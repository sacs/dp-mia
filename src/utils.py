from attack import attack
import numpy as np

def convergence_score(nodes):
    params = [i.get_serialized_model()['params'] for i in nodes]
    s, n = 0, len(nodes)
    if n == 1:
        return 0
    for i in range(n):
        for j in range(i):
            s += np.linalg.norm(params[i] - params[j])
    return 2*s / (n**2 - n)

def attack_list(nodes, test_loader, enable_attacks):
    results = []
    for i in nodes:
        attack_result = attack(i.model, i.device,
                               i.train_loader, test_loader, enable_attacks)
        best_attacker = attack_result.get_result_with_max_auc()
        results.append(best_attacker.roc_curve.get_auc())
    return results

def metrics(nodes, test_loader):
    accs = [i.test(test_loader) for i in nodes]
    privacy_scores = [i.get_privacy() for i in nodes]
    eps, alphas = zip(*privacy_scores)
    conv = convergence_score(nodes)
    return accs, eps, alphas, conv
