from model_cifar10 import SampleConvNet
from load_cifar10 import get_test
from small_world_graph import get_graph
from node import Node
from attack import attack
from itertools import product
from time import time
import numpy as np
import random as rd
import torch
import pandas as pd
from parser import parser, parse_lists
from utils import attack_list, metrics

torch.manual_seed(parser.seed)
np.random.seed(parser.seed)
rd.seed(parser.seed)
norms = parse_lists(parser.max_norms)
noises = parse_lists(parser.noise_multipliers)

graph = get_graph(n = parser.number_nodes)
n = len(graph)
test_loader = get_test()

columns = ['iter', 'loss', 'accuracy', 'convergence_score', 'time', 'epsilon', 'best_alpha', 
           'auc_attack_before', 'auc_attack_after', 'max_norm', 'noise_multiplier']

results = []
start = time()
for idx, (norm, noise) in enumerate(product(norms, noises)):
    print(f'Start of parameter {idx+1}/{len(norms)*len(noises)}')
    nodes = [Node(SampleConvNet, max_grad_norm = norm, noise_multiplier = noise,
                  training_size = 10000, dp = not parser.disable_dp) for _ in range(n)]
    n_iter = parser.n_iter
    for iter_ in range(n_iter):
        if parser.enable_dynamic:
            graph = get_graph(n = n, seed = rd.randint(0, 2**30))
        log = iter_ % 5 == 4 or parser.log_everything
        curr_nodes = list(range(n))
        #if it is federated learning, we only use a subset of the nodes in each iteration
        if parser.enable_fl:
            curr_nodes = list(rd.sample(curr_nodes, parser.nodes_per_iter))
            graph = [curr_nodes for i in range(n)]
        print(f'Iter: {iter_ + 1}/{n_iter}')
        params = {}
        losses = []
        #train the current nodes
        for i in curr_nodes:
            losses.append(nodes[i].train(epochs = parser.epochs))
            params[i] = nodes[i].get_state_dict()
        #attack nodes before updating
        if log:
            attacks_bfr = attack_list(nodes, test_loader, parser.enable_attacks)
        #update the node parameters
        for i in range(n):
            curr_params = []
            for j in graph[i]:
                curr_params.append(params[j])
            nodes[i].update_params(curr_params, include_own = not parser.enable_fl)
        #measure all metrics of interest
        if log:
            attacks_aft = attack_list(nodes, test_loader, parser.enable_attacks)
            print('Attacks before:', np.mean(attacks_bfr))
            print('Attacks after:', np.mean(attacks_aft))
            accs, eps, alphas, conv = metrics(nodes, test_loader)
            curr_result = [iter_+1, np.mean(losses), np.mean(accs), conv, time() - start, np.mean(eps),
                           np.mean(alphas), np.mean(attacks_bfr), np.mean(attacks_aft), norm, noise]
            results.append(curr_result)
            df = pd.DataFrame(results, columns = columns)
            df.to_csv(parser.results_file, index = False)
    print('---------------------------\n')
