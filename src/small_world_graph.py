import networkx as nx
import random as rd

def get_graph(n = 16, k = 4, p = 0.25, seed = 1):
    '''
    Get small world graph.

    Arguments:
        n (int, optional): number of nodes.
        k (int, optional): number of neighbors of each node.
        p (int, optional): probability of joining two random nodes.
        seed (int, optional): seed to random part of the algorithm.

    Returns:
        list: adjacency list representation of the generated graph.
    '''
    g = nx.watts_strogatz_graph(n, k, p, seed = seed)
    while not nx.is_connected(g):
        g = nx.watts_strogatz_graph(n, k, p, seed = rd.randint(0, 2**30))
    return [list(g[i]) for i in g]
