import torch
import numpy as np
from tqdm import tqdm
from scipy import special
from tensorflow.keras.backend import categorical_crossentropy as cce
from tensorflow.keras.backend import constant
from tensorflow_privacy.privacy.privacy_tests.membership_inference_attack.data_structures import AttackInputData
from tensorflow_privacy.privacy.privacy_tests.membership_inference_attack.data_structures import SlicingSpec
from tensorflow_privacy.privacy.privacy_tests.membership_inference_attack.data_structures import AttackType
from tensorflow_privacy.privacy.privacy_tests.membership_inference_attack import membership_inference_attack as mia

def get_data(model, device, loader, num_classes = 10):
    """
    Get the data in the right format for the attacks module.
    
    Arguments:
        model: the targeted model that will be attacked.
        device: the device where the model is going to be run in ('cuda' or 'cpu').
        loader: the data loader from where the function converts the data. 

    Returns:
        np.array: logit representation of running model with input from loader.
        np.array: loss values of running model with input from loader.
        np.array: labels returned by running model with input from loader.
    """
    model.eval()
    logit, prob, label = [], [], []
    with torch.no_grad():
        for (data, target) in loader:
            data, target = data.to(device), target.to(device)
            logit.append(np.array(model(data)))
            prob.append(np.array(special.softmax(logit[-1], axis = 1)))
            label.append(np.array(target))
    logit = np.concatenate(tuple(logit), axis = 0)
    prob = np.concatenate(tuple(prob), axis = 0)
    label = np.concatenate(tuple(label), axis = 0)
    y = []
    for i in label:
        y.append(np.array([0]*i + [1] + [0]*(num_classes-1-i)))
    y = np.array(y)
    loss = np.array(cce(constant(y), constant(prob), from_logits = False))
    return logit, loss, label

def attack(model, device, train_loader, test_loader, enable_attacks):
    """
    Run a black-box membership inference attack in the model using the data provided.

    Arguments:
        model: the targeted model that will be attacked.
        device: the device where the model is going to be run in ('cuda' or 'cpu').
        train_loader: data loader with the data used to train the model.
        test_loader: data loader with datat that was ***not*** used to train the model.
        enable_attacks: enable two other kinds of attacks

    Returns:
        AttackResults: logit representation of running model with input from loader.
    """
    logits_train, loss_train, labels_train = get_data(model, device, train_loader)
    logits_test, loss_test, labels_test = get_data(model, device, test_loader)
    input = AttackInputData(
                logits_train = logits_train,
                logits_test = logits_test,
                loss_train = loss_train,
                loss_test = loss_test,
                labels_train = labels_train,
                labels_test = labels_test)
    attack_types = [AttackType.THRESHOLD_ATTACK]
	# if the user wants to use more elaborate attacks
    if enable_attacks:
        attack_types = attack_types + [AttackType.THRESHOLD_ENTROPY_ATTACK]
    attacks_result = mia.run_attacks(input,
                                 SlicingSpec(
                                     entire_dataset = True,
                                     by_class = True,
                                     by_classification_correctness = True
                                 ),
                                 attack_types = attack_types)
    return attacks_result
