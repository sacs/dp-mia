import argparse

def parse_lists(arg):
    '''
    Convert strings received on the parser to floats or returns default value.

    Arguments:
        arg (list or None): list of strings or None (default value).

    Returns:
        list: list of floats with the parsed values.
    '''
    if arg is None:
        return [1.0]
    return [float(i) for i in arg]

parser = argparse.ArgumentParser()
parser.add_argument('--disable-dp', action = 'store_true', default = False, help = 'disable dp and train the model without provacy guarantees')
parser.add_argument('--seed', default = 0, type = int, help = 'random seed to initialize neural network')
parser.add_argument('--number-nodes', default = 16, type = int, help = 'number of nodes in the network')
parser.add_argument('--n-iter', default = 500, type = int, help = 'number of iterations')
parser.add_argument('--epochs', default = 5, type = int, help = 'number of training epochs between every round of sharing parameters')
parser.add_argument('--nodes-per-iter', default = 8, type = int, help = 'number of nodes that train on every iteration')
parser.add_argument('--enable-fl', action = 'store_true', default = False, help = 'enable federated learning instead of decentralized')
parser.add_argument('--enable-attacks', action = 'store_true', default = False, help = 'enable two more attacks')
parser.add_argument('--enable-dynamic', action = 'store_true', default = False, help = 'use a dynamic small world graph, instead of a static one')
parser.add_argument('--results-file', default = '../results/results.csv', type = str, help = 'file to save the results of the experiment')
parser.add_argument('--log-everything', action = 'store_true', default = False, help = 'log results every iteration')
parser.add_argument('--max-norms', nargs = '*', help = 'all the maximum gradient norms that are going to be tested')
parser.add_argument('--noise-multipliers', nargs = '*', help = 'all the noise multipliers that are going to be tested')

parser = parser.parse_args()
