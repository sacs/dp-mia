# dp-mia

This projects contain a simulated decentralized learning framework that can perform and measure the effectiveness of membership inference attacks (MIA).


## Description
The framework was originally made to test if the accuracy of [membership inference attacks (MIA)](https://arxiv.org/abs/1610.05820) decreases when [differential privacy (DP)](https://en.wikipedia.org/wiki/Differential_privacy) is used in a decentralized learning setup. After that, the code was extended to simulate federated learning and to make aggregations using [trusted execution environments (TEE)](https://en.wikipedia.org/wiki/Trusted_execution_environment). Some scripts analyzing some results will also be included.

## Installation
All the code was tested using Python 3.8.10. The `requirements.txt` file contains the minimum packages required to run the code. To install all of them with pip3, use ``pip3 install -r requirements.txt``.

## Usage
There are three main ways to use the library, decentralized learning, federated learning and aggregation with SGX. 

### Decentralized Learning
To run the code for decentralized learning, run the `main.py` script without the flag `--enable-fl`. For example, run `python3 main.py`. Running the script with the option `-h` will list all options. Because the parser is shared with other modes, the flag `nodes-per-iter` will not change anything.

### Federated Learning
To run the code for federated learning, run the `main.py` script with the flag `--enable-fl`. For example, run `python3 main.py --enable-fl`. Running the script with the option `-h` will list all options. 

### Aggregation with SGX
To run the code with SGX aggregation, run the `sgx.py`. For example, run `python3 sgx.py`. Running the script with the option `-h` will list all options. Because the parser is shared with other modes, the flags `number-nodes, nodes-per-iter, enable-fl, enable-dynamic` will not change anything. The graph is static and can only be changed inside the code, on lines 22-24. The topology used is shown in the image below. 

<center>
<img src="images/sgx_layout.png ">
</center>

### General characteristics
The original plan was to have a script that tests different combinations of parameters of DP. So, if the command `python3 main.py --max-norms 3 10 --noise-multipliers 3 5 10` is given, 6 experiments will be run, one for every combination of `max-norm` and `noise-multiplier`. Not using these options, makes the script run for only one combination of parameters.

The framework is modular, but not so much. If the user wants to use another neural network, they should create a file, like the file `model_cifar10`, and import like it is done in the file `main.py`. The same can be said about the graph and the dataset.

### Command line options
1. `disable-dp`: if this option is passed, the neural network will not use the [DP-SGD algorithm](https://medium.com/pytorch/differential-privacy-series-part-1-dp-sgd-algorithm-explained-12512c3959a3), it will use regular stochastic gradient descent.
1. `seed`: the seed used to initialize the neural network.
1. `number-nodes`: the number of nodes in the network. 
1. `n-iter`: number of iterations until execution finishes.
1. `epochs`: number of training epochs made by each node until a communication round begins.
1. `nodes-per-iter`: in federated learning, the number of nodes that train in each round between communications.
1. `enable-fl`: option to enable federated learning.
1. `enable-attacks`: more attacks, besides threshold attack, are used if this option is set.
1. `enable-dynamic`: this option makes the graph change after every communication round.
1. `results-file`: where all the results are stored.
1. `log-everything`: the standard behaviour is to measure accuracy and attacks every five communication rounds. This option makes this happen every iteration.
1. `max-norms`: The maximum norm of an update of the DP-SGD algorithm, as explained in [here](https://medium.com/pytorch/differential-privacy-series-part-1-dp-sgd-algorithm-explained-12512c3959a3).
1. `noise-multiplier`: the amount of noise that is added to each update on the DP-SGD algorithm, as explained in [here](https://medium.com/pytorch/differential-privacy-series-part-1-dp-sgd-algorithm-explained-12512c3959a3).
